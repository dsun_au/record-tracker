﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesOfTheStorm.RecordTracker.Shared;

public interface IResult
{
    bool IsSuccess { get; }
    IEnumerable<string> Errors { get; }
    IEnumerable<string> Warnings { get; }
    ErrorType ErrorType { get; }
}

public interface IResult<TValue> : IResult
{
    TValue? Value { get; }
}
