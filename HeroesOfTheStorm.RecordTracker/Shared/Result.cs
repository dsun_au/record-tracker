﻿namespace HeroesOfTheStorm.RecordTracker.Shared;

public class Result : IResult
{
    public bool IsSuccess => !Errors.Any();

    public IEnumerable<string> Errors { get; }

    public IEnumerable<string> Warnings { get; }

    public ErrorType ErrorType { get; }

    public Result(string[]? warnings = null)
    {
        Errors = Array.Empty<string>();
        Warnings = warnings ?? Array.Empty<string>();
    }

    public Result(string[] errors, ErrorType errorType = ErrorType.BadRequest, string[]? warnings = null)
    {
        Errors = errors ?? Array.Empty<string>();
        Warnings = warnings ?? Array.Empty<string>();
        ErrorType = errorType;
    }
}

public class Result<TValue> : Result, IResult<TValue>
{
    public TValue? Value { get; }

    public Result(string[] errors, ErrorType errorType = ErrorType.BadRequest, string[]? warnings = null) : base(errors, errorType, warnings)
    {
    }

    public Result(TValue value, string[]? warnings = null) : base(warnings)
    {
        Value = value;
    }
}
