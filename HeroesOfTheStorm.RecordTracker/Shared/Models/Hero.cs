﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models.HeroesOfTheStorm
{
    public class Hero
    {
        public Guid Id { get; init; }
        public Heroes Name { get; init; }

        public HeroClasses Class { get; init; }

        public BlizzardUniverses BlizzardUniverse { get; init; }

        public IEnumerable<string> Quests { get; init; } = new List<string>();
    }

    public enum HeroClasses
    {
        Tank,
        Bruiser,
        Support,
        Healer,
        [Display(Name = "Ranged Assassin")]
        RangedAssassin,
        [Display(Name = "Melee Assassin")]
        MeleeAssassin
    }

    public enum BlizzardUniverses
    {
        Warcraft,
        Starcraft,
        Diablo,
        Overwatch,
        [Display(Name = "Heroes of the Storm")]
        HeroesOfTheStorm,
        Other
    }

    public enum Heroes
    {
        Abathur,
        Alarak,
        Alexstrasza,
        Ana,
        Anduin,
        [Display(Name = "Anub'Arak")]
        Anub_arak,
        Artanis,
        Arthas,
        Auriel,
        Azmodan,
        Blaze,
        Brightwing,
        Cassia,
        Chen,
        Cho,
        [Display(Name = "Cho'Gall")]
        Cho_gall,
        Chromie,
        Deathwing,
        Deckard,
        Dehaka,
        Diablo,
        DVa,
        [Display(Name = "E.T.C")]
        ETC,
        Falstad,
        Fenix,
        Gall,
        Garrosh,
        Gazlowe,
        Genji,
        Greymane,
        [Display(Name = "Gul'Dan")]
        Gul_dan,
        Hanzo,
        Hogger,
        Illidan,
        Imperius,
        Jaina,
        Johanna,
        Junkrat,
        [Display(Name = "Kael'Thas")]
        Kael_thas,
        [Display(Name = "Kel'Thuzad")]
        Kel_Thuzad,
        Kerrigan,
        Kharazim,
        Leoric,
        [Display(Name = "Li-Ming")]
        Li_Ming,
        [Display(Name = "Li Li")]
        LiLi,
        [Display(Name = "Lt. Morales")]
        LtMorales,
        [Display(Name = "Lúcio")]
        Lucio,
        Lunara,
        Maiev,
        [Display(Name = "Mal'Ganis")]
        Mal_Ganis,
        Malfurion,
        Malthael,
        Medivh,
        Mei,
        Mephisto,
        Muradin,
        Murky,
        Nazeebo,
        Nova,
        Orphea,
        Probius,
        Qhira,
        Ragnaros,
        Raynor,
        Rehgar,
        Rexxar,
        Samuro,
        [Display(Name = "Sgt. Hammer")]
        Sgt_Hammer,
        Sonya,
        Stitches,
        Stukov,
        Sylvanas,
        Tassadar,
        [Display(Name = "The Butcher")]
        TheButcher,
        [Display(Name = "The Lost Vikings")]
        TheLostVikings,
        Thrall,
        Tracer,
        Tychus,
        Tyrael,
        Tyrande,
        Uther,
        Valeera,
        Valla,
        Varian,
        Whitemane,
        Xul,
        Yrel,
        Zagara,
        Zarya,
        Zeratul,
        [Display(Name = "Zul'Jin")]
        Zul_jin,

    }
}