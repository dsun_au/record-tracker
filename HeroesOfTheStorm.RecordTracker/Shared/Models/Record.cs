﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesOfTheStorm.RecordTracker.Shared.Models;
public abstract class Record
{
    public Guid Id { get; init; }
    public DateTime Time { get; init; }
    public string Notes { get; set; } = "";
    public Record(DateTime time)
    {
        Time = time;
    }
}

public class CountRecord : Record
{
    public int Count { get; }

    public CountRecord(DateTime time, int count) : base(time)
    {
        Count = count;
    }
}