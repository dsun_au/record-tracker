﻿namespace HeroesOfTheStorm.RecordTracker.Shared;

public enum ErrorType
{
    BadRequest,
    NotFound,
    ServerError
}
