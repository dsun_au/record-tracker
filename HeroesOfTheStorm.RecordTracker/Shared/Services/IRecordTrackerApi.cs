﻿using HeroesOfTheStorm.RecordTracker.Shared.Models;
using Refit;

namespace HeroesOfTheStorm.RecordTracker.Shared.Interfaces;
public interface IRecordTrackerApi
{

    [Get("/api/records")]
    public Task<IApiResponse<Record[]>> GetRecords([Query]GetPageOptions getPageOptions);

    [Get("/api/records/{id}")]
    public Task<IApiResponse<Record>> GetRecord([Query] Guid id);
}
