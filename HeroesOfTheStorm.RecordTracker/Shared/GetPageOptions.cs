﻿namespace HeroesOfTheStorm.RecordTracker.Shared;
public class GetPageOptions
{
    public int PageNumber { get; init; } = 0;
    public int Limit { get; init; } = 0;
    public PageFilter[] Filters { get; init; } = Array.Empty<PageFilter>(); 
}

public class PageFilter
{
    public string Name { get; }
    public FilterOperator Operator { get; }
    public string Value { get; }

    public PageFilter(string name, string value, FilterOperator filterOperator = FilterOperator.Equals)
    {
        Name = name;
        Value = value;
        Operator = filterOperator;
    }
}

public enum FilterOperator
{
    Equals,
    NotEquals,
    GreaterThan,
    LessThan,
    GreaterThanOrEqual,
    LessThanOrEqual,
    Contains
}
