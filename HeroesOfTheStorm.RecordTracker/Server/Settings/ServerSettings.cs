﻿namespace HeroesOfTheStorm.RecordTracker.Server.Settings;

public class ServerSettings
{
    public string MongoConnectionString { get; init; } = "";

    public string MongoDatabaseName { get; init; } = "";

    public ServerSettings()
    {

    }

}
