﻿using HeroesOfTheStorm.RecordTracker.Business.Services;
using HeroesOfTheStorm.RecordTracker.Shared;
using HeroesOfTheStorm.RecordTracker.Shared.Models;
using HeroesOfTheStorm.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace HeroesOfTheStorm.RecordTracker.Server.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RecordsController : Controller
{
    private readonly IRecordService recordService;

    public RecordsController(IRecordService recordService)
    {
        this.recordService = recordService;
    }

    [HttpGet]
    public async Task<ActionResult<Record[]>> GetRecords([FromQuery] GetPageOptions getPageOptions)
    {
        var result = await recordService.GetRecords(getPageOptions);
        return result.ToActionResult();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Record>> GetRecord([FromRoute] Guid id)
    {
        var result = await recordService.GetRecord(id);
        return result.ToActionResult();
    }

    [HttpPut]
    public async Task<IActionResult> PutRecords([FromBody]Record record)
    {
        var result = await recordService.SaveRecord(record);
        return result.ToActionResult();
    }
}
