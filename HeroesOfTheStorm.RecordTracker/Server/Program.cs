﻿using HeroesOfTheStorm.RecordTracker.Business.Services;
using HeroesOfTheStorm.RecordTracker.Server.Settings;
using Microsoft.AspNetCore.Hosting.StaticWebAssets;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

StaticWebAssetsLoader.UseStaticWebAssets(builder.Environment, builder.Configuration);

var serverSettingsConfiguration = builder.Configuration.GetSection(nameof(ServerSettings));
var serverSettings = serverSettingsConfiguration.Get<ServerSettings>() ?? throw new InvalidOperationException();

builder.Services.Configure<ServerSettings>(serverSettingsConfiguration);

// Add services to the container.
builder.Services.AddSingleton<IRecordService, RecordService>();

var mongoClientSettings = MongoClientSettings.FromConnectionString(serverSettings.MongoConnectionString);
mongoClientSettings.ServerApi = new ServerApi(ServerApiVersion.V1);
var database = new MongoClient(mongoClientSettings).GetDatabase(serverSettings.MongoDatabaseName);
builder.Services.AddSingleton(database);

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

var port = Environment.GetEnvironmentVariable("PORT") ?? null;
var url = port is not null ? $"http://0.0.0.0:{port}" : null;
Console.WriteLine(url is null ? "No port provided by variable PORT. " : $"URL is: {url}");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseOpenApi(); // serve documents (same as app.UseSwagger())
app.UseSwaggerUi3(settings => settings.DocumentTitle = "Check Lists API"); // serve Swagger UI

app.UseRouting();


app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

if (url is null)
    app.Run();
else
    app.Run(url);