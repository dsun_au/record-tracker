using HeroesOfTheStorm.RecordTracker.Client;
using HeroesOfTheStorm.RecordTracker.Shared.Interfaces;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;
using Refit;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddMudServices();

var checkListsApi = RestService.For<IRecordTrackerApi>($"{builder.HostEnvironment.BaseAddress}");
builder.Services.AddSingleton(checkListsApi);

await builder.Build().RunAsync();