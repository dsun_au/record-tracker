﻿using HeroesOfTheStorm.RecordTracker.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IResult = HeroesOfTheStorm.RecordTracker.Shared.IResult;

namespace HeroesOfTheStorm.Utilities;
public static class ResultExtensions
{
    public static IActionResult ToActionResult(this IResult result)
    {
        if (result.IsSuccess)
            return new OkResult();

        return result.ErrorType switch
        {
            ErrorType.BadRequest => new BadRequestResult(),
            ErrorType.NotFound => new NotFoundResult(),
            ErrorType.ServerError => throw new NotImplementedException(),
            _ => throw new ArgumentOutOfRangeException(),
        };
    }

    public static ActionResult<TValue> ToActionResult<TValue>(this IResult<TValue> result)
    {
        if (result.IsSuccess)
            return new OkObjectResult(result.Value);

        return result.ErrorType switch
        {
            ErrorType.BadRequest => new BadRequestResult(),
            ErrorType.NotFound => new NotFoundResult(),
            ErrorType.ServerError => new ObjectResult(null) { StatusCode = StatusCodes.Status500InternalServerError },
            _ => throw new ArgumentOutOfRangeException(),
        };
    }
}
