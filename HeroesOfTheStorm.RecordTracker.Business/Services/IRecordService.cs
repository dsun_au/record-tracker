﻿using HeroesOfTheStorm.RecordTracker.Shared;
using HeroesOfTheStorm.RecordTracker.Shared.Models;

namespace HeroesOfTheStorm.RecordTracker.Business.Services;
public interface IRecordService
{
    Task<IResult<Record>> GetRecord(Guid id);
    Task<IResult<Record[]>> GetRecords(GetPageOptions getPageOptions);
    Task<IResult> SaveRecord(Record record);
}