﻿using HeroesOfTheStorm.RecordTracker.Shared;
using HeroesOfTheStorm.RecordTracker.Shared.Models;
using MongoDB.Driver;

namespace HeroesOfTheStorm.RecordTracker.Business.Services;
public class RecordService : IRecordService
{
    private readonly IMongoDatabase mongoDatabase;
    private readonly FilterDefinitionBuilder<Record> filterBuilder = new();
    private readonly UpdateDefinitionBuilder<Record> updateBuilder = new();
    private readonly SortDefinitionBuilder<Record> sortBuilder = new();

    public RecordService(IMongoDatabase mongoDatabase)
    {
        this.mongoDatabase = mongoDatabase;
    }

    protected IMongoCollection<Record> GetCollection() => mongoDatabase.GetCollection<Record>("Records");

    public async Task<IResult<Record[]>> GetRecords(GetPageOptions getPageOptions)
    {
        var collection = GetCollection();

        var filter = filterBuilder.Empty;
        var sort = sortBuilder.Descending(x => x.Time);
        var cursor = await collection.FindAsync(filter, new FindOptions<Record, Record> { BatchSize = getPageOptions.Limit });
        
        for (var i = 1; i < getPageOptions.PageNumber; i++)
            await cursor.MoveNextAsync();

        avar records = cursor.Current.ToArray();
        return new Result<Record[]>(records);
    }

    public async Task<IResult<Record>> GetRecord(Guid id)
    {
        var collection = GetCollection();
        var filter = filterBuilder.Eq(record => record.Id, id);
        var cursor = await collection.FindAsync(filter);

        return new Result<Record>(cursor.Single());
    }

    public async Task<IResult> SaveRecord(Record record)
    {
        var collection = GetCollection();
        var result = await collection.ReplaceOneAsync(record => record.Id == record.Id, record, new ReplaceOptions { IsUpsert = true });
        if (result.IsAcknowledged && (result.UpsertedId is not null || result.ModifiedCount > 0))
            return new Result();
        else
            return new Result(new[] { "Record saving failed" }, ErrorType.ServerError);
    }
}
