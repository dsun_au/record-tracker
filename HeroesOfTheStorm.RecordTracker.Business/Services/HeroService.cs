﻿using HeroesOfTheStorm.RecordTracker.Shared;
using MongoDB.Driver;
using RecordTrackingApplication.Models.HeroesOfTheStorm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesOfTheStorm.RecordTracker.Business.Services;
public class HeroService
{
    private readonly IMongoDatabase mongoDatabase;
    private readonly FilterDefinitionBuilder<Hero> filterBuilder = new();
    private readonly UpdateDefinitionBuilder<Hero> updateBuilder = new();

    public HeroService(IMongoDatabase mongoDatabase)
    {
        this.mongoDatabase = mongoDatabase;
    }

    protected IMongoCollection<Hero> GetCollection() => mongoDatabase.GetCollection<Hero>("Heroes");

    public async Task<IResult<Hero>> GetHeroes()
    {
        var collection = GetCollection();

        var cursor = await collection.FindAsync(filterBuilder.Empty);
        var heroes = cursor.ToList();

        return heroes.Count == 0
            ? new Result<Hero>(new[] { "Couldn't find any heroes." }, ErrorType.NotFound)
            : new Result<Hero>(heroes.Single());
    }

    public async Task<IResult<Hero>> GetHero(Guid id)
    {
        var collection = GetCollection();

        var filter = filterBuilder.Eq(hero => hero.Id, id);

        var cursor = await collection.FindAsync(filter);
        var heroes = cursor.ToList();

        return heroes.Count == 0 || heroes.Single() == null
            ? new Result<Hero>(new[] { "Couldn't find hero." }, ErrorType.NotFound)
            : new Result<Hero>(heroes.Single());
    }

    public async Task GetHero(string name)
    {
        throw new NotImplementedException();
    }

    public async Task SaveHero(Hero hero)
    {
        throw new NotImplementedException();
    }

    public async Task DeleteHero(Guid id)
    {
        throw new NotImplementedException();
    }
}
